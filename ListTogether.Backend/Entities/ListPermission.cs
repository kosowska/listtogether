using System.Text.Json.Serialization;
using SQLite;

namespace ListTogether.Backend.Entities
{
    [Table("listpermission")]
    public class ListPermission
    {
        [PrimaryKey][AutoIncrement]
        [Column("id")]
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        [JsonPropertyName("user_id")]
        [Column("userId")]
        public int UserId { get; set; } 
        
        [JsonPropertyName("list_id")]
        [Column("listId")]
        public int ListId { get; set; } 
        
        [JsonPropertyName("permission_type")]
        [Column("permissionType")] 
        public string PermissionType { get; set; }
    }
}