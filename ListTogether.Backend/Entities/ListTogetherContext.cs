using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using SQLite;


namespace ListTogether.Backend.Entities
{
    public class ListTogetherContext : DbContext
    {

        public ListTogetherContext(DbContextOptions connString): base(connString)
        {

        }
        public ListTogetherContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=/Users/klaudiuszek/Desktop/listtogether/ListTogetherDatabase.sqlite3;");//"Data Source=./test2.sqlite3;");
        public DbSet<User> User { get; set; }
        public DbSet<List> List { get; set; }
        public DbSet<ListPermission> ListPermission { get; set; }
    }
}