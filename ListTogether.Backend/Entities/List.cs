using System.Collections.Generic;
using System.Text.Json.Serialization;
using SQLite;

namespace ListTogether.Backend.Entities
{
    [Table("list")]
    public class List
    {
        [PrimaryKey][AutoIncrement]
        [Column("id")]
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        [Column("name")]
        [JsonPropertyName("name")]
        public string Name { get; set; }
        
        [Column("description")]
        [JsonPropertyName("description")]
        public string Description { get; set; }
        
        [Column("items")]
        [JsonPropertyName("items")]
        public string Items { get; set; }
    }
}