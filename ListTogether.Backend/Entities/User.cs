using System.Text.Json.Serialization;
using SQLite;

namespace ListTogether.Backend.Entities
{
    [Table("user")]
    public class User
    {
        
        [PrimaryKey][AutoIncrement]
        [Column("id")]
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        [JsonPropertyName("name")]
        [Column("name")]
        public string Name { get; set; } 
        
        [JsonPropertyName("password")]
        [Column("password")]
        public string Password { get; set; }
        
        [JsonPropertyName("email")]
        [Column("email")] 
        public string Email { get; set; } //unique
    }
}