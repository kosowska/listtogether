using System;
using System.Data.Entity;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using SQLite;

namespace ListTogether.Backend.Entities
{
    public class DbManager
    {
        //  private SqliteConnection connection;
        private ListTogetherContext myctx;

        public DbManager()
        {
            myctx = new ListTogetherContext();
        }

       
        private static DbContextOptions GetOptions(string connectionString)
        {
            return SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), connectionString).Options;
        }

        public async Task<User> GetUserIfExist(User u)
        {
            return  await myctx.User.Where(x => x.Email == u.Email && x.Password == u.Password).FirstOrDefaultAsync();
        }

        public async Task AddUserToDatabase(User u)
        {
            myctx.Add(u);
            myctx.SaveChanges();
        }
        public async Task AddListToDatabase(List list)
        {
            myctx.Add(list);
            myctx.SaveChanges();
        }
        public async Task AddOrChangeUserListPermission(User user, List list, string permissionType)
        {
            var permission = new ListPermission
            {
                ListId = list.Id, UserId = user.Id, PermissionType = permissionType
            };
            var permissionFromDb = await myctx.ListPermission
                .Where(x => x.ListId == permission.ListId && x.UserId == permission.UserId && x.PermissionType == permissionType).FirstOrDefaultAsync();
           if(permissionFromDb == null)
           {
                myctx.Add(permission);
           }
           else
           {
               myctx.Update(permission);
           }
           await myctx.SaveChangesAsync();
        }
        
        public async Task<bool> AddNewPermission(string userEmail, int listId, string permissionType)
        {
            var permission = new ListPermission
            {
                ListId = listId, 
                UserId = (await myctx.User.FirstAsync(x => x.Email == userEmail)).Id,
                PermissionType = permissionType
            };
            var permissionFromDb = await myctx.ListPermission
                .Where(x => x.ListId == permission.ListId 
                            && x.UserId == permission.UserId 
                            && x.PermissionType == permissionType).FirstOrDefaultAsync();
            
            if(permissionFromDb != null)
            {
                return false;
            }
            await myctx.AddAsync(permission);
            await myctx.SaveChangesAsync();
            return true;
        }
        
        public async Task<bool> ChangeExistingPermission(string userEmail, int listId, string permissionType)
        {
            var permission = new ListPermission
            {
                ListId = listId, 
                UserId = (await myctx.User.FirstAsync(x => x.Email == userEmail)).Id,
                PermissionType = permissionType
            };
            var permissionFromDb = await myctx.ListPermission
                .Where(x => x.ListId == permission.ListId 
                            && x.UserId == permission.UserId 
                            && x.PermissionType == permissionType).FirstOrDefaultAsync();
            
            if(permissionFromDb == null)
            {
                return false;
            }
            myctx.Update(permission);
            await myctx.SaveChangesAsync();
            return true;
        }

        public async Task DeleteListPermission(User user, List list)
        {
            var permission = myctx.ListPermission.FirstOrDefaultAsync(x => x.ListId == list.Id && x.UserId == user.Id);
            if (permission != null)
            {
                myctx.Remove(permission);
                await myctx.SaveChangesAsync();
            }
        }
        
        public List GetListById(int listId)
        {
            return myctx.List.First(x => x.Id == listId);
        }

        public List<int> GetUsersListsIds(int userId)
        {
            return myctx.ListPermission.Where(x => x.UserId == userId).Select(x => x.ListId).ToList();
        }
    }
}