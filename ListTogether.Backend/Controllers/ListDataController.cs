using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Web.Http.Description;
using System.Web.Http.Hosting;
using ListTogether.Backend.Entities;
using Microsoft.AspNetCore.Http;
using Npgsql;


namespace ListTogether.Backend.Controllers
{
    [ApiController]
    [Microsoft.AspNetCore.Mvc.Route("[controller]")]
    public class ListDataController: ApiController
    {
        private DbManager db;
        private JsonSerializerOptions serializerOptions = new JsonSerializerOptions();
        public ListDataController()
        {
            db = new DbManager();
        }

        [Microsoft.AspNetCore.Mvc.HttpGet]
        [Microsoft.AspNetCore.Mvc.Route("/listtogether/login/{userSerialized}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<User> > GetIfUserExist(string userSerialized)
        {
            User uFrontendData = JsonSerializer.Deserialize<User>(userSerialized, serializerOptions);
            User user = await db.GetUserIfExist(uFrontendData);
            if (user == null)
            {
                return new NotFoundResult();
            }
            return user;
        }
        
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [Microsoft.AspNetCore.Mvc.Route("/listtogether/signup/{userSerialized}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<User> > SignUpUser(string userSerialized)
        {
            User uFrontendData = JsonSerializer.Deserialize<User>(userSerialized, serializerOptions);
            db.AddUserToDatabase(uFrontendData);
            return new OkResult();
        }
        
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [Microsoft.AspNetCore.Mvc.Route("/listtogether/addnewlist/{userSerialized}/{listSerialized}/{permission}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<User> > AddNewList(string userSerialized, string listSerialized, string permission)
        {
            User user = JsonSerializer.Deserialize<User>(userSerialized, serializerOptions);
            List list = JsonSerializer.Deserialize<List>(listSerialized, serializerOptions);
            db.AddListToDatabase(list);
            db.AddOrChangeUserListPermission(user, list, permission);
            return new OkResult();
        }
        
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [Microsoft.AspNetCore.Mvc.Route("/listtogether/addnewpermission/{userEmail}/{listId:int}/{permission}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<bool> > AddNewPermission(string userEmail, int listId, string permission)
        {
            if (await db.AddNewPermission(userEmail, listId, permission))
            {
                return new OkResult();
            }

            return new BadRequestResult();
        }
        
        public async Task<ActionResult<bool> > ChangeExistingPermission(string userEmail, int listId, string permissionType)
        {
            if (await db.ChangeExistingPermission(userEmail, listId, permissionType))
            {
                return new OkResult();
            }

            return new BadRequestResult();
        }

        
        //AddOrChangeUserPermission (user, list, permissiontype)
        //DeleteUserPermission(user, list) -> usuwa uprawnienia uzytkownika do listy
        //DeleteList(list) ->usuwa liste i wszystkie uprawnienia do tej listy
        //
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [Microsoft.AspNetCore.Mvc.Route("/listtogether/getlistbyid/{listId:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List> > GetListById(int listId)
        {
            return db.GetListById(listId);
        }
        
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [Microsoft.AspNetCore.Mvc.Route("/listtogether/getuserslists/{userId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IList> > GetUsersLists(int userId)
        {
            var result = new List<List>();
            var usersListsIds = db.GetUsersListsIds(userId);
            foreach (var usersListsId in usersListsIds)
            {
                result.Add(db.GetListById(usersListsId));
            }

            return result;
        }
        
    }
}