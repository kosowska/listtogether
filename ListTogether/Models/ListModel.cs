﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using ListTogether.Services;
using Xamarin.Forms;

namespace ListTogether.Models
{
    [Serializable]
    public class ListModel
    {
        public enum PermissionType
        {
            Admin,
            Guest
        }
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("description")]
        public string Description { get; set; }
        [JsonPropertyName("items")]
        public string Items { get; set; }// => DependencyService.Get<IDataStore<Item>>();
    }
}
