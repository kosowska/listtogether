﻿using System;

namespace ListTogether.Models
{
    public class Item
    {
        public int ListId { get; set; }
        public int ItemId { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
    }
}
