﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using ListTogether.Services;
using Microsoft.EntityFrameworkCore;
using Xamarin.Forms;

namespace ListTogether.Models
{
    [Serializable]
    public class UserModel 
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } //unique
        [JsonPropertyName("password")]
        public string Password { get; set; }
        [JsonPropertyName("email")]
        public string Email { get; set; } //unique
        [JsonPropertyName("id")]
        public int Id { get; set; }
    }
}
