using System.Text.Json.Serialization;

namespace ListTogether.Models
{
    public class ListPermissionModel
    {
        public class ListPermission
        {
            [JsonPropertyName("id")]
            public int Id { get; set; }
        
            [JsonPropertyName("user_id")]
            public int UserId { get; set; } 
        
            [JsonPropertyName("list_id")]
            public int ListId { get; set; } 
        
            [JsonPropertyName("permission_type")]
            public ListModel.PermissionType PermissionType { get; set; }
        }
    }
}