﻿using System;
using System.Collections.Generic;
using ListTogether.Models;
using ListTogether.ViewModels;

using Xamarin.Forms;

namespace ListTogether.Views
{
    public partial class NewListPage : ContentPage
    {
        public ListModel List { get; set; }

        public NewListPage()
        {
            InitializeComponent();
            BindingContext = new NewListViewModel();
        }
    }
}
