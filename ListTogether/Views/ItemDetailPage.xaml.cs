﻿using System.ComponentModel;
using Xamarin.Forms;
using ListTogether.ViewModels;

namespace ListTogether.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}
