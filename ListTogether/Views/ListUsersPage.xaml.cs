﻿using System;
using System.Collections.Generic;
using ListTogether.ViewModels;
using Xamarin.Forms;

namespace ListTogether.Views
{
    public partial class ListUsersPage : ContentPage
    {
        public ListUsersPage()
        {
            InitializeComponent();
            BindingContext = new ListUsersViewModel();
        }
    }
}
