﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using ListTogether.ViewModels;
using Xamarin.Forms;

namespace ListTogether.Views
{
    public partial class ListPage : ContentPage
    {
        ListViewModel _viewModel;
        public ListPage()
        {
            InitializeComponent();
            BindingContext = _viewModel = new ListViewModel();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }

        public void OnCheckedChanged(object s, CheckedChangedEventArgs e)
        {
            if (((CheckBox)s)?.Parent is StackLayout)
            {
                StackLayout parent = (StackLayout)((CheckBox)s)?.Parent;
                if(parent?.Children[0] is StackLayout)
                {
                    StackLayout itemInfo = (StackLayout)parent?.Children[0];
                    if (itemInfo?.FindByName("TextLabel") is Label)
                    {
                        Label textLabel = (Label)itemInfo?.FindByName("TextLabel");
                        if (e.Value)
                        {
                            textLabel.TextDecorations = TextDecorations.Strikethrough;
                        }
                        else
                        {
                            textLabel.TextDecorations = TextDecorations.None;
                        }
                    }
                }
                
            }
        
        }


    }
}
