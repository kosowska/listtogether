﻿using System;
using System.Collections.Generic;
using ListTogether.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace ListTogether.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WelcomePage : ContentPage
    {
        public WelcomePage()
        {
            InitializeComponent();
            this.BindingContext = new WelcomeViewModel();
        }
    }
}
