﻿using System;
using System.Collections.Generic;
using ListTogether.ViewModels;
using Xamarin.Forms;

namespace ListTogether.Views
{
    public partial class ListsManagerPage : ContentPage
    {
        ListsManagerViewModel _viewModel;
        public ListsManagerPage()
        {
           /* Shell.SetBackButtonBehavior(this, new BackButtonBehavior
            {
                TextOverride = "Logout"
            });*/
           // NavigationPage.SetHasBackButton(this, false);
            InitializeComponent();
           // NavigationPage.SetHasBackButton(this, false);
            BindingContext = _viewModel = new ListsManagerViewModel();
        }
        protected override void OnAppearing()
        {
           // NavigationPage.SetHasBackButton(this, false);
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}
