﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ListTogether.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ListTogether.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            NavigationPage.SetHasBackButton(this, false);
            InitializeComponent();
            this.BindingContext = new LoginViewModel();
        }
    }
}
