﻿using System;
using System.Collections.Generic;
using ListTogether.ViewModels;
using Xamarin.Forms;

namespace ListTogether.Views
{
    public partial class ListSettingsPage : ContentPage
    {
        public ListSettingsPage()
        {
            InitializeComponent();
            BindingContext = new ListSettingsViewModel();
        }
    }
}
