﻿using System;
using System.Collections.Generic;
using ListTogether.ViewModels;
using Xamarin.Forms;

namespace ListTogether.Views
{
    public partial class UserSettingsPage : ContentPage
    {
        public UserSettingsPage()
        {
            InitializeComponent();
            BindingContext = new UserSettingsViewModel();
        }
    }
}
