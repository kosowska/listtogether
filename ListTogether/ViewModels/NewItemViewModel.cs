﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using ListTogether.Models;
using ListTogether.Services;
using Xamarin.Forms;

namespace ListTogether.ViewModels
{
    [QueryProperty(nameof(ListId), nameof(ListId))]
    public class NewItemViewModel : BaseViewModel
    {
        private string text;
        private string description;
        public int ListId { get; set; }

        public NewItemViewModel()
        {
            Description = "";
            SaveCommand = new Command(OnSave, ValidateSave);
            CancelCommand = new Command(OnCancel);
            this.PropertyChanged +=
                (_, __) => SaveCommand.ChangeCanExecute();
        }

        private bool ValidateSave()
        {
            return !String.IsNullOrWhiteSpace(text);
        }

        public string Text
        {
            get => text;
            set => SetProperty(ref text, value);
        }

        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }

        public Command SaveCommand { get; }
        public Command CancelCommand { get; }

        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }

        private async void OnSave()
        {
            Item newItem = new Item()
            {
                ItemId = 0,
                Text = Text,
                Description = Description,
                ListId = ListId
            };
            var myList = await ListsContainer.AllLists.GetItemAsync(ListId);
           // await myList.Items.AddItemAsync(newItem);
            
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }
    }
}
