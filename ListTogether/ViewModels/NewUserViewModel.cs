using System;
using System.Collections.Generic;
using System.Linq;
using ListTogether.Models;
using ListTogether.RestServices;
using Xamarin.Forms;

namespace ListTogether.ViewModels
{
    [QueryProperty(nameof(ListId), nameof(ListId))]
    public class NewUserViewModel : BaseViewModel
    {
        private RestService rs => DependencyService.Get<RestService>();
        private ListModel.PermissionType selectedPermission = ListModel.PermissionType.Guest;

        public ListModel.PermissionType SelectedPermission
        {
            get => selectedPermission;
            set
            {
                selectedPermission = value;
                OnPropertyChanged(nameof(SelectedPermission));
            }
        }

        public List<ListModel.PermissionType> PermissionsList =>
            Enum.GetValues(typeof(ListModel.PermissionType)).Cast<ListModel.PermissionType>().ToList();

        public Command AddUserCommand { get; }

        private string email;
        private int listId;

        public string Email
        {
            get => email;
            set => SetProperty(ref email, value);
        }

        public int ListId
        {
            get => listId;
            set => SetProperty(ref listId, value);
        }
        public NewUserViewModel()
        {
            AddUserCommand = new Command(OnAddUser);
        }

        private async void OnAddUser()
        {
            bool wasAdded = await rs.AddNewPermission(Email, listId, SelectedPermission);
            if (!wasAdded)
            {
                await App.Current.MainPage.DisplayAlert("Error", 
                    "Adding user failed. Check if email address is correct.", "Ok");
            }
        }
    }
}