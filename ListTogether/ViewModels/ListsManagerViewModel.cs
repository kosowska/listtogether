﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using ListTogether.Models;
using ListTogether.Services;
using ListTogether.Views;
using Xamarin.Forms;

namespace ListTogether.ViewModels
{
    public class ListsManagerViewModel : BaseViewModel
    {
        private ListModel _selectedList;
       // public ListsDataStore DataStore; //=> DependencyService.Get<IDataStore<T>>();
        public ObservableCollection<ListModel> Lists { get; }
        public Command LoadListsCommand { get; }
        public Command AddListCommand { get; }
        public Command<ListModel> ListTapped { get; }
        public ListsManagerViewModel()
        {
            Title = "ListsManager";
            Lists = new ObservableCollection<ListModel>();
            LoadListsCommand = new Command(async () => await ExecuteLoadListsCommand());
            ListTapped = new Command<ListModel>(OnListSelected);
            AddListCommand = new Command(OnAddList);
        }

        async Task ExecuteLoadListsCommand()
        {
            IsBusy = true;

            try
            {
                Lists.Clear();
                var items = await ListsContainer.AllLists.GetItemsAsync(true);
                foreach (var item in items)
                {
                    Lists.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedList = null;
        }

        public ListModel SelectedList
        {
            get => _selectedList;
            set
            {
                SetProperty(ref _selectedList, value);
                OnListSelected(value);
            }
        }
        private async void OnAddList(object obj)
        {
            await Shell.Current.GoToAsync(nameof(NewListPage));
        }

        async void OnListSelected(ListModel list)
        {
            if (list == null)
                return;

            // This will push the ItemDetailPage onto the navigation stack
            await Shell.Current.GoToAsync($"{nameof(ListPage)}?{nameof(ListViewModel.ListId)}={list.Id}");
        }
    }
}

