﻿using ListTogether.Models;
using ListTogether.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ListTogether.ViewModels
{
    public class WelcomeViewModel : BaseViewModel
    {
        public Command LoginCommand { get; }
        public Command SignUpCommand { get; }

        public WelcomeViewModel()
        {
            LoginCommand = new Command(OnLoginClicked);
            SignUpCommand = new Command(OnSignUpClicked);
        }

        private async void OnLoginClicked(object obj)
        { 
            await Shell.Current.GoToAsync($"{nameof(LoginPage)}");
        }
        private async void OnSignUpClicked(object obj)
        {
            await Shell.Current.GoToAsync($"{nameof(SignupPage)}");
        }
    }
}

