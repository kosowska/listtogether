﻿using Xamarin.Forms;
using ListTogether.Models;
using TodoApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ListTogether.ViewModels
{
    [QueryProperty(nameof(loggedUserModel), nameof(loggedUserModel))]
    public static class LoggedUser
    {
        
        public static UserModel loggedUserModel = new UserModel();
        public static void SetLoggedUser(UserModel user)
        {
            DbContextOptionsBuilder<UsersContext> _optionsBuilder = new DbContextOptionsBuilder<UsersContext>();

            var context = new UsersContext(_optionsBuilder.Options);
            //USTAW W MENU LISTY KTORE NALEZA DO TEGO GOSCIA  (INFO Z BAZY)

            loggedUserModel = user;
        }
         
    }
}