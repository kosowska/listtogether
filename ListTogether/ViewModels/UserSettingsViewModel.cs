﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using ListTogether.Models;
using ListTogether.Views;

namespace ListTogether.ViewModels
{
    
    public class UserSettingsViewModel : BaseViewModel
    {
        private string email;
        private string password;
        private string name;
        

        public UserSettingsViewModel()
        {
            LoggedUser.loggedUserModel.Name = "Klaudia";
            LoggedUser.loggedUserModel.Email = "claudia@gmail.com";
            LoggedUser.loggedUserModel.Password = "haslo1";
            Title = "Account settings";
            email = LoggedUser.loggedUserModel.Email;
            password = new string('*', LoggedUser.loggedUserModel.Password.Length);
            name = LoggedUser.loggedUserModel.Name;
        }

        public string Email
        {
            get => email;
            set => SetProperty(ref email, value);
        }
        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }
        public string Password
        {
            get => password;
            set => SetProperty(ref password, value);
        }
    }
}
