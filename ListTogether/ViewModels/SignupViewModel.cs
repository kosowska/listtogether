using ListTogether.Models;
using ListTogether.RestServices;
using ListTogether.Views;
using Xamarin.Forms;

namespace ListTogether.ViewModels
{
    public class SignupViewModel : BaseViewModel
    {
        public Command SignUpCommand { get; }
        private RestService rs => DependencyService.Get<RestService>();

        private string email, password, confirmPassword, name;
        
        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }
        public string Email
        {
            get => email;
            set => SetProperty(ref email, value);
        }

        public string Password
        {
            get => password;
            set => SetProperty(ref password, value);
        }
        
        public string ConfirmPassword
        {
            get => confirmPassword;
            set => SetProperty(ref confirmPassword, value);
        }

        private bool AreEnteredDataValid { get; set; }

        public SignupViewModel()
        {
            SignUpCommand = new Command(OnSignUpClicked);
            
        }

        private (bool isOk, string message) ValidateEnteredData()
        {
            if (Email.Contains("%"))
            {
                return (false, "Email is incorrect");
            }

            if (Password != ConfirmPassword)
            {
                return (false, "Passwords are not identical");
            }

            return (true, "");
        }
        private async void OnSignUpClicked()
        {
            string msg;
            (AreEnteredDataValid, msg)  = ValidateEnteredData();
            if (!AreEnteredDataValid)
            {
                await App.Current.MainPage.DisplayAlert("Signing up failed", msg, "Ok");
                return;
            }
            UserModel user = new UserModel();
            user.Email = Email;
            user.Password = Password;
            user.Name = Name;
            var (userExists, userFromDataBase) = await rs.GetUserIfExist(user);

            if (userExists)
            {
                await App.Current.MainPage.DisplayAlert("Signing up failed", "User with this email already exists.", "Ok");
            }
            
            var signupSuccess = await rs.SignUpUser(user);
            if (!signupSuccess)
            {
                await App.Current.MainPage.DisplayAlert("Signing up failed", "Sign up failed, try again.", "Ok");
                return;
            }
            LoggedUser.SetLoggedUser(user);
            await Shell.Current.GoToAsync($"{nameof(ListsManagerPage)}");
            
        }
    }
}
