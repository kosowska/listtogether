﻿using ListTogether.Models;
using ListTogether.Views;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TodoApi.Models;
using Xamarin.Forms;
using System.Linq;
using Npgsql;
using ListTogether.RestServices;

namespace ListTogether.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        public Command LogInCommand { get; }
        private RestService rs => DependencyService.Get<RestService>();


        private string email, password;

        public string Email
        {
            get => email;
            set => SetProperty(ref email, value);
        }

        public string Password
        {
            get => password;
            set => SetProperty(ref password, value);
        }

        private bool AreEnteredDataValid { get; set; }

        public LoginViewModel()
        {
            LogInCommand = new Command(OnLogInClicked);
            Email = "claudia@gmail.com";
            Password = "kokob1";
        }

        private bool ValidateEnteredData()
        {
            return true;
        }
        private async void OnLogInClicked()
        {
            AreEnteredDataValid = ValidateEnteredData();
            if (!AreEnteredDataValid)
            {
                //popup -> mail jest w niepoprawnym formacie
            }
            UserModel user = new UserModel();
            user.Email = email;
            user.Password = password;
            var (exists, userData) = await rs.GetUserIfExist(user);

            if (exists)
            {
                LoggedUser.SetLoggedUser(userData);
                
                await Shell.Current.GoToAsync($"{nameof(ListsManagerPage)}");
            }
            else
            {
                await App.Current.MainPage.DisplayAlert("Login failed", "Email or password is incorrect", "Ok");
            }
        }
    }
}
