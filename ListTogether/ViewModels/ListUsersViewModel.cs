﻿

using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using ListTogether.Models;
using ListTogether.RestServices;
using ListTogether.Services;
using ListTogether.Views;
using Xamarin.Forms;

namespace ListTogether.ViewModels
{
    [QueryProperty(nameof(ListId), nameof(ListId))]
    public class ListUsersViewModel : BaseViewModel
    {
        private int listId;
        private string listName;

        public int ListId
        {
            get => listId;
            set => listId = value;
        }
        
        private RestService rs => DependencyService.Get<RestService>();
        //private Item _selectedItem;
        public ObservableCollection<UserModel> Users { get; }
       // public IDataStore<Item> DataStore => DependencyService.Get<IDataStore<Item>>();
        
        public Command LoadUsersCommand { get; }
        public Command AddUserCommand { get; }
        public Command ListSettingsCommand { get; }
       // public Command ManageUsersCommand { get; }
        public Command<UserModel> UserTapped { get; }
       // public EventHandler OnCheckedChanged;

        public ListUsersViewModel()
        {
            //Title = Name;
            Users = new ObservableCollection<UserModel>();
            LoadUsersCommand = new Command(async () => await ExecuteLoadUsersCommand());
            //ListSettingsCommand = new Command(OnListSettings);
            //ManageUsersCommand = new Command(OnUsersClicked);

            UserTapped = new Command<UserModel>(OnUserSelected);

            AddUserCommand = new Command(OnAddUser);
        }

        private async void OnUsersClicked()
        {
            //await Shell.Current.GoToAsync($"{nameof(ListUsersPage)}");
        }

        async Task ExecuteLoadUsersCommand()
        {
            IsBusy = true;

            try
            {
                Users.Clear();
                var lists = await rs.GetUsersWithAccessToList(listId);
                foreach (var list in lists)
                {
                    Users.Add(list);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

       /* public void OnAppearing()
        {
            IsBusy = true;
            SelectedItem = null;
        }

        public Item SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
                OnUserSelected(value);
            }
        }*/

        private async void OnAddUser(object obj)
        {
            await Shell.Current.GoToAsync($"{nameof(NewUserPage)}?{nameof(NewUserViewModel.ListId)}={ListId}");
        }

        async void OnUserSelected(UserModel user)
        {
            if (user == null)
                return;

            // This will push the ItemDetailPage onto the navigation stack
          //  await Shell.Current.GoToAsync($"{nameof(ItemDetailPage)}?{nameof(ItemDetailViewModel.ListId)}={user.Id}&{nameof(ItemDetailViewModel.ItemId)}={user.ItemId}");
        }

        // public void CheckBoxChecked(System.Object sender, Xamarin.Forms.CheckedChangedEventArgs e)
        // {
        // }
        public void OnCheckedChanged(object s, EventArgs e)
        {
        }

        public string Name
        {
            get => listName;
            set => SetProperty(ref listName, value);
        }

    }

}
