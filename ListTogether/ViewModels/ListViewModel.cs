﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using ListTogether.Models;
using ListTogether.Services;
using ListTogether.Views;
using Xamarin.Forms;
using System.Windows;

namespace ListTogether.ViewModels
{
    [QueryProperty(nameof(ListId), nameof(ListId))]
    public class ListViewModel : BaseViewModel
    {
        private int listId;
        private string listName;
        private string description;

        private Item _selectedItem;
        public ObservableCollection<Item> Items { get; }
        public IDataStore<Item> DataStore => DependencyService.Get<IDataStore<Item>>();
        
        public Command LoadItemsCommand { get; }
        public Command AddItemCommand { get; }
        public Command ListSettingsCommand { get; }
        public Command ManageUsersCommand { get; }
        public Command<Item> ItemTapped { get; }
       // public EventHandler OnCheckedChanged;

        public ListViewModel()
        {
            Title = Name;
            Items = new ObservableCollection<Item>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
            ListSettingsCommand = new Command(OnListSettings);
            ManageUsersCommand = new Command(OnUsersClicked);

            ItemTapped = new Command<Item>(OnItemSelected);

            AddItemCommand = new Command(OnAddItem);
        }

        private async void OnUsersClicked()
        {
            await Shell.Current.GoToAsync($"{nameof(ListUsersPage)}?{nameof(ListUsersViewModel.ListId)}={listId}");
        }

        async Task ExecuteLoadItemsCommand()
        {
            IsBusy = true;

            try
            {
                Items.Clear();
                var listModel = await ListsContainer.AllLists.GetItemAsync(listId);
                /*var items = await listModel.Items.GetItemsAsync(true);
                foreach (var item in items)
                {
                    Items.Add(item);
                }*/
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedItem = null;
        }

        public Item SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
                OnItemSelected(value);
            }
        }

        private async void OnAddItem(object obj)
        {
            await Shell.Current.GoToAsync($"{nameof(NewItemPage)}?{nameof(NewItemViewModel.ListId)}={listId}");
        }

        async void OnItemSelected(Item item)
        {
            if (item == null)
                return;

            // This will push the ItemDetailPage onto the navigation stack
            await Shell.Current.GoToAsync($"{nameof(ItemDetailPage)}?{nameof(ItemDetailViewModel.ListId)}={item.ListId}&{nameof(ItemDetailViewModel.ItemId)}={item.ItemId}");
        }

        // public void CheckBoxChecked(System.Object sender, Xamarin.Forms.CheckedChangedEventArgs e)
        // {
        // }
        public void OnCheckedChanged(object s, EventArgs e)
        {
        }

        public string Name
        {
            get => listName;
            set => SetProperty(ref listName, value);
        }

        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }

        public int ListId
        {
            get
            {
                return listId;
            }
            set
            {
                listId = value;
                LoadListId(value);
            }
        }

        public async void LoadListId(int listId)
        {
            try
            {
                var list = await ListsContainer.AllLists.GetItemAsync(ListId);
                Name = list.Name;
                Description = list.Description;
                Title = Name;
            }
            catch (Exception)
            {
                Debug.WriteLine("Failed to Load List");
            }
        }
        async void OnListSettings()
        {
            await Shell.Current.GoToAsync($"{nameof(ListSettingsPage)}");
        }

    }
}
