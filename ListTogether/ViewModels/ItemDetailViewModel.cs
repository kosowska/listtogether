﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using ListTogether.Models;
using ListTogether.Services;
using Xamarin.Forms;

namespace ListTogether.ViewModels
{
    [QueryProperty(nameof(ItemId), nameof(ItemId))]
    [QueryProperty(nameof(ListId), nameof(ListId))]
    public class ItemDetailViewModel : BaseViewModel
    {
        private int listId;
        private int itemId;
        private string text;
        private string description;

        public ItemDetailViewModel()
        {

        }

        public string Text
        {
            get => text;
            set => SetProperty(ref text, value);
        }

        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }

        public int ItemId
        {
            get => itemId;
            set => SetProperty(ref itemId, value);
        }
        public int ListId
        {
            get => listId;
            set
            {
                SetProperty(ref listId, value);
                LoadItemId(ItemId);
            }
        }

        public async void LoadItemId(int itemId)
        {
            try
            {
                var myList = await ListsContainer.AllLists.GetItemAsync(ListId);
                //var item = await myList.Items.GetItemAsync(itemId);
                //Text = item.Text;
                //Description = item.Description;
            }
            catch (Exception)
            {
                Debug.WriteLine("Failed to Load Item");
            }
        }
    }
}
