﻿using System;
using ListTogether.Models;
using ListTogether.RestServices;
using ListTogether.Services;
using Xamarin.Forms;

namespace ListTogether.ViewModels
{
    public class NewListViewModel : BaseViewModel
    {
        private string name;
        private string description;
        private RestService rs => DependencyService.Get<RestService>();


        public NewListViewModel()
        {
            Description = "";
            SaveCommand = new Command(OnSave, ValidateSave);
            CancelCommand = new Command(OnCancel);
            this.PropertyChanged +=
                (_, __) => SaveCommand.ChangeCanExecute();
        }
        

        private bool ValidateSave()
        {
            return !String.IsNullOrWhiteSpace(name);
        }

        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }

        public Command SaveCommand { get; }
        public Command CancelCommand { get; }

        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }

        private async void OnSave()
        {
            ListModel newList = new ListModel()
            {
                Id = 0,
                Name = Name,
                Description = Description,
               // Items = new MockDataStore()
               Items = ""
            };
            bool addList = await rs.AddNewList(LoggedUser.loggedUserModel, newList);
            await ListsContainer.AllLists.AddItemAsync(newList);
            
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }
    }
}
   
