﻿using ListTogether.Models;
using Xamarin.Forms;

namespace ListTogether.Services
{
    public static class ListsContainer
    {
        public static IDataStore<ListModel> AllLists => DependencyService.Get<IDataStore<ListModel>>();

    }
}