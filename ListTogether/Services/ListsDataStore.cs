﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ListTogether.Models;

namespace ListTogether.Services
{
    public class ListsDataStore : IDataStore<ListModel>
    {
        readonly List<ListModel> lists;

        public ListsDataStore()
        {
            lists = new List<ListModel>() { };
        }

        public async Task<bool> AddItemAsync(ListModel list)
        {
            lists.Add(list);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(ListModel list)
        {
            var oldList = lists.Where((ListModel arg) => arg.Id == list.Id).FirstOrDefault();
            lists.Remove(oldList);
            lists.Add(list);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(int id)
        {
            var oldList = lists.Where((ListModel arg) => arg.Id == id).FirstOrDefault();
            lists.Remove(oldList);

            return await Task.FromResult(true);
        }

        public async Task<ListModel> GetItemAsync(int id)
        {
            return await Task.FromResult(lists.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<ListModel>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(lists);
        }
    }
}
